﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using WebServerProva.WS_List_Employees;
using WebServerProva.WS_List_Stapling_No;
using WebServerProva.WS_List_User_Work_Center;
using WebServerProva.WS_Machine_Center_List;
using WebServerProva.WS_Manufacturing_Functions;
using WebServerProva.WS_List_Stapling_QMgt;
using WebServerProva.WS_Scrap_Codes;
using WebServerProva.ProvaWS;
using WebServerProva.WS_Get_Scrap_Lot;
using WebServerProva.WS_Scrap_Comp_Reason_Codes;
using WebServerProva.WS_Rework_Scrap_Codes;
using WebServerProva.WS_Scrap_Comp_Codes;
using WebServerProva.WS_Finished_Reason_Codes;
using WebServerProva.WS_Terminal_Setup;

namespace WebServerProva
{
    class SimpleHTTPServer
    {

        private static IDictionary<string, string> _mimeTypeMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
        #region extension to MIME type list
        {".asf", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".avi", "video/x-msvideo"},
        {".bin", "application/octet-stream"},
        {".cco", "application/x-cocoa"},
        {".crt", "application/x-x509-ca-cert"},
        {".css", "text/css"},
        {".deb", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dll", "application/octet-stream"},
        {".dmg", "application/octet-stream"},
        {".ear", "application/java-archive"},
        {".eot", "application/octet-stream"},
        {".exe", "application/octet-stream"},
        {".flv", "video/x-flv"},
        {".gif", "image/gif"},
        {".hqx", "application/mac-binhex40"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".ico", "image/x-icon"},
        {".img", "application/octet-stream"},
        {".iso", "application/octet-stream"},
        {".jar", "application/java-archive"},
        {".jardiff", "application/x-java-archive-diff"},
        {".jng", "image/x-jng"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".mml", "text/mathml"},
        {".mng", "video/x-mng"},
        {".mov", "video/quicktime"},
        {".mp3", "audio/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpg", "video/mpeg"},
        {".msi", "application/octet-stream"},
        {".msm", "application/octet-stream"},
        {".msp", "application/octet-stream"},
        {".pdb", "application/x-pilot"},
        {".pdf", "application/pdf"},
        {".pem", "application/x-x509-ca-cert"},
        {".pl", "application/x-perl"},
        {".pm", "application/x-perl"},
        {".png", "image/png"},
        {".prc", "application/x-pilot"},
        {".ra", "audio/x-realaudio"},
        {".rar", "application/x-rar-compressed"},
        {".rpm", "application/x-redhat-package-manager"},
        {".rss", "text/xml"},
        {".run", "application/x-makeself"},
        {".sea", "application/x-sea"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".swf", "application/x-shockwave-flash"},
        {".tcl", "application/x-tcl"},
        {".tk", "application/x-tcl"},
        {".txt", "text/plain"},
        {".war", "application/java-archive"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wmv", "video/x-ms-wmv"},
        {".xml", "text/xml"},
        {".xpi", "application/x-xpinstall"},
        {".zip", "application/zip"},
        #endregion
    };
        private Thread _serverThread;
        private string _rootDirectory;
        private HttpListener _listener;
        private int _port;

        public int Port
        {
            get { return _port; }
            private set { }
        }

        /// <summary>
        /// Construct server with given port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        /// <param name="port">Port of the server.</param>
        public SimpleHTTPServer(string path, int port)
        {
            this.Initialize(path, port);
        }

        /// <summary>
        /// Construct server with suitable port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        public SimpleHTTPServer(string path)
        {

            //get an empty port
            TcpListener l = new TcpListener(IPAddress.Any, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            this.Initialize(path, port);
        }

        /// <summary>
        /// Stop server and dispose all functions.
        /// </summary>
        public void Stop()
        {
            _serverThread.Abort();
            _listener.Stop();
        }

        private void Listen()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add("http://+:" + _port.ToString() + "/");
            try
            {
                _listener.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Accesso Negato avvia come amministratore");
            }
            while (true)
            {
                try
                {
                    HttpListenerContext context = _listener.GetContext();
                    Thread thread = new Thread(delegate () {
                        Process(context);
                    });/*new ThreadStart(WorkThreadFunction));*/
                    thread.Start();
                    //Process(context);
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void Process(HttpListenerContext context)
        {
            string filename = context.Request.Url.AbsolutePath;
            string type = context.Request.HttpMethod;

            int errGET = 0;
            int errPOST = 0;
            int errOPTIONS = 0;


            StreamReader reader = new StreamReader(context.Request.InputStream);
            string text = reader.ReadToEnd();
            Console.WriteLine(filename+"|"+ type+"|"+text);

            if (type == "GET")
            {
                /******************* ProvaWSArray *******************/
                if (filename.CompareTo("/ProvaWSArray") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    ProvaWS.ProvaWs_PortClient myItem = new ProvaWs_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {

                        string[] Code1 = new string[1];
                        Code1[0] = "";

                        string[] Text1 = new string[1];
                        Text1[0] = "";

                        string[] Code10 = new string[10];
                        Code10[0] = "";
                        Code10[1] = "";
                        Code10[2] = "";
                        Code10[3] = "";
                        Code10[4] = "";
                        Code10[5] = "";
                        Code10[6] = "";
                        Code10[7] = "";
                        Code10[8] = "";
                        Code10[9] = "";

                        string[] Text10 = new string[10];
                        Text10[0] = "";
                        Text10[1] = "";
                        Text10[2] = "";
                        Text10[3] = "";
                        Text10[4] = "";
                        Text10[5] = "";
                        Text10[6] = "";
                        Text10[7] = "";
                        Text10[8] = "";
                        Text10[9] = "";


                        string[] wCode1 = new string[5];
                        string[] wTest1 = new string[5];
                        string[] wCode10 = new string[5];
                        string[] wTest10 = new string[5];

                        for (int i = 0; i < 5; i++) {
                            wCode1[i] = "";
                            wTest1[i] = "";
                            wCode10[i] = "";
                            wTest10[i] = "";
                        }


                        myItem.TestWSARRAY(ref wCode1, ref wTest1
                            , ref wCode10, ref wTest10);

                        JObject oJsonObject = new JObject();

                        // GESTIONE ARRAY LOTQTY
                        JArray ArrayLotQt = new JArray();

                        JArray ArrayLotQty1 = new JArray();

                        for (int i = 0; i < wCode1.Length; i++)
                        {
                                ArrayLotQty1.Add(wCode1[i]);

                        }

                        ArrayLotQt.Add(ArrayLotQty1);

                        JArray ArrayLotQty2 = new JArray();

                        for (int i = 0; i < wTest1.Length; i++)
                        {
                                ArrayLotQty2.Add(wTest1[i]);

                        }

                        ArrayLotQt.Add(ArrayLotQty2);


                        JArray ArrayLotQty3 = new JArray();

                        for (int i = 0; i < wCode10.Length; i++)
                        {
                                ArrayLotQty3.Add(wCode10[i]);

                        }

                        ArrayLotQt.Add(ArrayLotQty3);


                        JArray ArrayLotQty4 = new JArray();

                        for (int i = 0; i < wTest10.Length; i++)
                        {
                                ArrayLotQty4.Add(wTest10[i]);

                        }

                        ArrayLotQt.Add(ArrayLotQty4);

                        oJsonObject.Add("WS_Array", ArrayLotQt);


                        Stream input;
                        using (input = GenerateStreamFromString(oJsonObject.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }
                }
            
                /******************* WS_List_Employees *******************/
                if (filename.CompareTo("/WS_List_Employees")==0)
                {

                    WS_List_Employees.WS_List_Employees[] newItemsInterface = new WS_List_Employees.WS_List_Employees[1];
                    WS_List_Employees_PortClient myItem = new WS_List_Employees_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {
                            
                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Field_Type", newItemsInterface[i].Field_Type.ToString());
                            obj.Add("Field_TypeSpecified", newItemsInterface[i].Field_TypeSpecified);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);
                            //Console.WriteLine(newItemsInterface[i]. + " " + newItemsInterface[i].Field_Type /*+ " "+ newItemsInterface[i].Key*/);

                        }
                        //System.Threading.Thread.Sleep(100000);
                        //oJsonObject.Add(array);

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_List_Stapling_No *******************/
                if (filename.CompareTo("/WS_List_Stapling_No") == 0)
                {

                    WS_List_Stapling_No.WS_List_Stapling_No[] newItemsInterface = new WS_List_Stapling_No.WS_List_Stapling_No[1];
                    WS_List_Stapling_No_PortClient myItem = new WS_List_Stapling_No_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        var query = context.Request.Url.Query;
                        string TxtFilter = System.Web.HttpUtility.ParseQueryString(query).Get("Filter");

                        List<WS_List_Stapling_No_Filter> myFiltersList = new List<WS_List_Stapling_No_Filter>();
                        if (!string.IsNullOrEmpty(TxtFilter))
                        {
                            WS_List_Stapling_No_Filter myFilter = new WS_List_Stapling_No_Filter();
                            myFilter.Field = WS_List_Stapling_No_Fields.Stapling_Code;
                            myFilter.Criteria = TxtFilter.ToUpper();

                            myFiltersList.Add(myFilter);
                        }

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Item_Description", newItemsInterface[i].Item_Description);
                            obj.Add("Item_No", newItemsInterface[i].Item_No);
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Operation_No", newItemsInterface[i].Operation_No);
                            obj.Add("Production_Date", newItemsInterface[i].Production_Date);
                            obj.Add("Production_DateSpecified", newItemsInterface[i].Production_DateSpecified);
                            obj.Add("Production_Sort", newItemsInterface[i].Production_Sort);
                            obj.Add("Production_SortSpecified", newItemsInterface[i].Production_SortSpecified);
                            obj.Add("Prod_Order_No", newItemsInterface[i].Prod_Order_No);
                            obj.Add("Remaining_Quantity", newItemsInterface[i].Remaining_Quantity);
                            obj.Add("Remaining_QuantitySpecified", newItemsInterface[i].Remaining_QuantitySpecified);
                            obj.Add("Routing_Link_Code", newItemsInterface[i].Routing_Link_Code);
                            obj.Add("Routing_No", newItemsInterface[i].Routing_No);
                            obj.Add("Stapling_Code", newItemsInterface[i].Stapling_Code);
                            obj.Add("Starting_Date", newItemsInterface[i].Starting_Date.ToString("dd/MM/yyyy HH:mm:ss"));
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
			                obj.Add("CustomerNo", newItemsInterface[i].CustomerNo);
			                obj.Add("CustomerName", newItemsInterface[i].CustomerName);
                            array.Add(obj);

                            

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message+"|"+ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_List_User_Work_Center *******************/
                if (filename.CompareTo("/WS_List_User_Work_Center") == 0)
                {

                    WS_List_User_Work_Center.WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_List_User_Work_Center_PortClient myItem = new WS_List_User_Work_Center_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Name", newItemsInterface[i].Name);
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_Machine_Center_List *******************/
                if (filename.CompareTo("/WS_Machine_Center_List") == 0)
                {

                    WS_Machine_Center_List.WS_Machine_Center_List[] newItemsInterface = new WS_Machine_Center_List.WS_Machine_Center_List[1];
                    WS_Machine_Center_List_PortClient myItem = new WS_Machine_Center_List_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Name", newItemsInterface[i].Name);
                            obj.Add("No", newItemsInterface[i].No);
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }
                /******************* WS_List_Stapling_QMgt *******************/
                if (filename.CompareTo("/WS_List_Stapling_QMgt") == 0)
                {

                    WS_List_Stapling_QMgt.WS_List_Stapling_QMgt[] newItemsInterface = new WS_List_Stapling_QMgt.WS_List_Stapling_QMgt[1];
                    WS_List_Stapling_QMgt_PortClient myItem = new WS_List_Stapling_QMgt_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    Console.WriteLine("AAAAAAAAAAAAAAA");
                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("CalcProducedQty", newItemsInterface[i].CalcProducedQty);
                            obj.Add("CalcProducedQtySpecified", newItemsInterface[i].CalcProducedQtySpecified);
                            obj.Add("Completed_Qty", newItemsInterface[i].Completed_Qty);
                            obj.Add("Completed_QtySpecified", newItemsInterface[i].Completed_QtySpecified);
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Operation_No", newItemsInterface[i].Operation_No);
                            obj.Add("Order_Qty", newItemsInterface[i].Order_Qty);
                            obj.Add("Order_QtySpecified", newItemsInterface[i].Order_QtySpecified);
                            obj.Add("Priority", newItemsInterface[i].Priority);
                            obj.Add("PrioritySpecified", newItemsInterface[i].PrioritySpecified);
                            obj.Add("Prod_Order_No", newItemsInterface[i].Prod_Order_No);
                            obj.Add("Quantity", newItemsInterface[i].Quantity);
                            obj.Add("QuantitySpecified", newItemsInterface[i].QuantitySpecified);
                            obj.Add("Routing_No", newItemsInterface[i].Routing_No);
                            obj.Add("Stapling_No", newItemsInterface[i].Stapling_No);
                            obj.Add("Terminal_Name", newItemsInterface[i].Terminal_Name);
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_Scrap_Codes *******************/
                if (filename.CompareTo("/WS_Scrap_Codes") == 0)
                {

                    WS_Scrap_Codes.WS_Scrap_Codes[] newItemsInterface = new WS_Scrap_Codes.WS_Scrap_Codes[1];
                    WS_Scrap_Codes_PortClient myItem = new WS_Scrap_Codes_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        var query = context.Request.Url.Query;
                        string TxtFilter = System.Web.HttpUtility.ParseQueryString(query).Get("Filter");

                        List<WS_Scrap_Codes_Filter> myFiltersList = new List<WS_Scrap_Codes_Filter>();
                        if (!string.IsNullOrEmpty(TxtFilter))
                        {
                            WS_Scrap_Codes_Filter myFilter = new WS_Scrap_Codes_Filter();
                            myFilter.Field = WS_Scrap_Codes_Fields.Internal_Code;
                            myFilter.Criteria = TxtFilter.ToUpper();

                            myFiltersList.Add(myFilter);
                        }

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Description", newItemsInterface[i].Description);
                            //obj.Add("Field_Type", newItemsInterface[i].Field_Type);
                            obj.Add("Field_TypeSpecified", newItemsInterface[i].Field_TypeSpecified);
                            obj.Add("Internal_Caption", newItemsInterface[i].Internal_Caption);
                            obj.Add("Internal_Code", newItemsInterface[i].Internal_Code);
                            obj.Add("Internal_Code_2", newItemsInterface[i].Internal_Code_2);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + "|" + ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_Get_Scrap_Lot *******************/
                if (filename.CompareTo("/WS_Get_Scrap_Lot") == 0)
                {

                    WS_Get_Scrap_Lot.WS_Get_Scrap_Lot[] newItemsInterface = new WS_Get_Scrap_Lot.WS_Get_Scrap_Lot[1];
                    WS_Get_Scrap_Lot_PortClient myItem = new WS_Get_Scrap_Lot_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        var query = context.Request.Url.Query;
                        string TxtFilterTerm = System.Web.HttpUtility.ParseQueryString(query).Get("Terminal");
                        string TxtFilterArea = System.Web.HttpUtility.ParseQueryString(query).Get("WorkCenter");
                        string TxtFilterPinz = System.Web.HttpUtility.ParseQueryString(query).Get("Stapling");
                        string TxtFilterCicl = System.Web.HttpUtility.ParseQueryString(query).Get("Routing");

                        List<WS_Get_Scrap_Lot_Filter> myFiltersList = new List<WS_Get_Scrap_Lot_Filter>();
                        if (!string.IsNullOrEmpty(TxtFilterTerm))
                        {
                            WS_Get_Scrap_Lot_Filter myFilter = new WS_Get_Scrap_Lot_Filter();
                            myFilter.Field = WS_Get_Scrap_Lot_Fields.Terminal_Name;
                            myFilter.Criteria = TxtFilterTerm.ToUpper();

                            myFiltersList.Add(myFilter);
                        }
                        if (!string.IsNullOrEmpty(TxtFilterArea))
                        {
                            WS_Get_Scrap_Lot_Filter myFilter = new WS_Get_Scrap_Lot_Filter();
                            myFilter.Field = WS_Get_Scrap_Lot_Fields.Work_Center_No;
                            myFilter.Criteria = TxtFilterArea.ToUpper();

                            myFiltersList.Add(myFilter);
                        }
                        if (!string.IsNullOrEmpty(TxtFilterPinz))
                        {
                            WS_Get_Scrap_Lot_Filter myFilter = new WS_Get_Scrap_Lot_Filter();
                            myFilter.Field = WS_Get_Scrap_Lot_Fields.Stapling_No;
                            myFilter.Criteria = TxtFilterPinz.ToUpper();

                            myFiltersList.Add(myFilter);
                        }
                        if (!string.IsNullOrEmpty(TxtFilterCicl))
                        {
                            WS_Get_Scrap_Lot_Filter myFilter = new WS_Get_Scrap_Lot_Filter();
                            myFilter.Field = WS_Get_Scrap_Lot_Fields.Routing_No;
                            myFilter.Criteria = TxtFilterCicl.ToUpper();

                            myFiltersList.Add(myFilter);
                        }

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Bin_Content", newItemsInterface[i].Bin_Content);
                            obj.Add("Bin_ContentSpecified", newItemsInterface[i].Bin_ContentSpecified);
                            obj.Add("Current_Pending_Quantity", newItemsInterface[i].Current_Pending_Quantity);
                            obj.Add("Current_Pending_QuantitySpecified", newItemsInterface[i].Current_Pending_QuantitySpecified);
                            obj.Add("Current_Reserved_Quantity", newItemsInterface[i].Current_Reserved_Quantity);
                            obj.Add("Current_Reserved_QuantitySpecified", newItemsInterface[i].Current_Reserved_QuantitySpecified);
                            obj.Add("Entry_No", newItemsInterface[i].Entry_No);
                            obj.Add("Entry_NoSpecified", newItemsInterface[i].Entry_NoSpecified);
                            obj.Add("Expiration_Date", newItemsInterface[i].Expiration_Date);
                            obj.Add("Expiration_DateSpecified", newItemsInterface[i].Expiration_DateSpecified);
                            obj.Add("fBinContentProductionLotQuantity", newItemsInterface[i].fBinContentProductionLotQuantity);
                            obj.Add("fBinContentProductionLotQuantitySpecified", newItemsInterface[i].fBinContentProductionLotQuantitySpecified);
                            obj.Add("fProductionLotQuantity", newItemsInterface[i].fProductionLotQuantity);
                            obj.Add("fProductionLotQuantitySpecified", newItemsInterface[i].fProductionLotQuantitySpecified);
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Lot_No", newItemsInterface[i].Lot_No);
                            obj.Add("Receipt_Date", newItemsInterface[i].Receipt_Date);
                            obj.Add("Receipt_DateSpecified", newItemsInterface[i].Receipt_DateSpecified);
                            obj.Add("Routing_No", newItemsInterface[i].Routing_No);
                            obj.Add("Selected_Quantity", newItemsInterface[i].Selected_Quantity);
                            obj.Add("Selected_QuantitySpecified", newItemsInterface[i].Selected_QuantitySpecified);
                            obj.Add("Serial_No", newItemsInterface[i].Serial_No);
                            obj.Add("Stapling_No", newItemsInterface[i].Stapling_No);
                            obj.Add("Terminal_Name", newItemsInterface[i].Terminal_Name);
                            obj.Add("Total_Available_Quantity", newItemsInterface[i].Total_Available_Quantity);
                            obj.Add("Total_Available_QuantitySpecified", newItemsInterface[i].Total_Available_QuantitySpecified);
                            obj.Add("Total_Quantity", newItemsInterface[i].Total_Quantity);
                            obj.Add("Total_QuantitySpecified", newItemsInterface[i].Total_QuantitySpecified);
                            obj.Add("Total_Requested_Quantity", newItemsInterface[i].Total_Requested_Quantity);
                            obj.Add("Total_Requested_QuantitySpecified", newItemsInterface[i].Total_Requested_QuantitySpecified);
                            obj.Add("Total_Reserved_Quantity", newItemsInterface[i].Total_Reserved_Quantity);
                            obj.Add("Total_Reserved_QuantitySpecified", newItemsInterface[i].Total_Reserved_QuantitySpecified);
                            obj.Add("Warranty_Date", newItemsInterface[i].Warranty_Date);
                            obj.Add("Warranty_DateSpecified", newItemsInterface[i].Warranty_DateSpecified);
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + "|" + ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }


                /******************* WS_Scrap_Comp_Reason_Codes *******************/
                if (filename.CompareTo("/WS_Scrap_Comp_Reason_Codes") == 0)
                {

                    WS_Scrap_Comp_Reason_Codes.WS_Scrap_Comp_Reason_Codes[] newItemsInterface = new WS_Scrap_Comp_Reason_Codes.WS_Scrap_Comp_Reason_Codes[1];
                    WS_Scrap_Comp_Reason_Codes_PortClient myItem = new WS_Scrap_Comp_Reason_Codes_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        List<WS_Scrap_Comp_Reason_Codes_Filter> myFiltersList = new List<WS_Scrap_Comp_Reason_Codes_Filter>();

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Description", newItemsInterface[i].Description);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + "|" + ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                /******************* WS_Rework_Scrap_Codes *******************/
                if (filename.CompareTo("/WS_Rework_Scrap_Codes") == 0)
                {

                    WS_Rework_Scrap_Codes.WS_Rework_Scrap_Codes[] newItemsInterface = new WS_Rework_Scrap_Codes.WS_Rework_Scrap_Codes[1];
                    WS_Rework_Scrap_Codes_PortClient myItem = new WS_Rework_Scrap_Codes_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        List<WS_Rework_Scrap_Codes_Filter> myFiltersList = new List<WS_Rework_Scrap_Codes_Filter>();

                        var query = context.Request.Url.Query;
                        string TxtFilterICode = System.Web.HttpUtility.ParseQueryString(query).Get("Internal_Code");
                        string TxtFilterICaption = System.Web.HttpUtility.ParseQueryString(query).Get("Internal_Caption");

                        if (!string.IsNullOrEmpty(TxtFilterICode))
                        {
                            WS_Rework_Scrap_Codes_Filter myFilter = new WS_Rework_Scrap_Codes_Filter();
                            myFilter.Field = WS_Rework_Scrap_Codes_Fields.Internal_Code;
                            myFilter.Criteria = TxtFilterICode.ToUpper();

                            myFiltersList.Add(myFilter);
                        }
                        if (!string.IsNullOrEmpty(TxtFilterICaption))
                        {
                            WS_Rework_Scrap_Codes_Filter myFilter = new WS_Rework_Scrap_Codes_Filter();
                            myFilter.Field = WS_Rework_Scrap_Codes_Fields.Internal_Caption;
                            myFilter.Criteria = TxtFilterICaption.ToUpper();

                            myFiltersList.Add(myFilter);
                        }

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Description", newItemsInterface[i].Description);
                            //obj.Add("Field_Type", newItemsInterface[i].Field_Type.ToString());
                            obj.Add("Field_TypeSpecified", newItemsInterface[i].Field_TypeSpecified);
                            obj.Add("Internal_Caption", newItemsInterface[i].Internal_Caption);
                            obj.Add("Internal_Code", newItemsInterface[i].Internal_Code);
                            obj.Add("Internal_Code_2", newItemsInterface[i].Internal_Code_2);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + "|" + ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }


                /******************* WS_Scrap_Comp_Codes *******************/
                if (filename.CompareTo("/WS_Scrap_Comp_Codes") == 0)
                {

                    WS_Scrap_Comp_Codes.WS_Scrap_Comp_Codes[] newItemsInterface = new WS_Scrap_Comp_Codes.WS_Scrap_Comp_Codes[1];
                    WS_Scrap_Comp_Codes_PortClient myItem = new WS_Scrap_Comp_Codes_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowNtlm = true;
                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    try
                    {
                        List<WS_Scrap_Comp_Codes_Filter> myFiltersList = new List<WS_Scrap_Comp_Codes_Filter>();

                        var query = context.Request.Url.Query;
                        string TxtFilterICode = System.Web.HttpUtility.ParseQueryString(query).Get("Internal_Code");
                        string TxtFilterICaption = System.Web.HttpUtility.ParseQueryString(query).Get("Internal_Caption");

                        if (!string.IsNullOrEmpty(TxtFilterICode))
                        {
                            WS_Scrap_Comp_Codes_Filter myFilter = new WS_Scrap_Comp_Codes_Filter();
                            myFilter.Field = WS_Scrap_Comp_Codes_Fields.Internal_Code;
                            myFilter.Criteria = TxtFilterICode.ToUpper();

                            myFiltersList.Add(myFilter);
                        }
                        if (!string.IsNullOrEmpty(TxtFilterICaption))
                        {
                            WS_Scrap_Comp_Codes_Filter myFilter = new WS_Scrap_Comp_Codes_Filter();
                            myFilter.Field = WS_Scrap_Comp_Codes_Fields.Internal_Caption;
                            myFilter.Criteria = TxtFilterICaption.ToUpper();

                            myFiltersList.Add(myFilter);
                        }

                        newItemsInterface = myItem.ReadMultiple(myFiltersList.ToArray(), null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Description", newItemsInterface[i].Description);
                            //obj.Add("Field_Type", newItemsInterface[i].Field_Type.ToString());
                            obj.Add("Field_TypeSpecified", newItemsInterface[i].Field_TypeSpecified);
                            obj.Add("Internal_Caption", newItemsInterface[i].Internal_Caption);
                            obj.Add("Internal_Code", newItemsInterface[i].Internal_Code);
                            obj.Add("Internal_Code_2", newItemsInterface[i].Internal_Code_2);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + "|" + ex.InnerException);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                if (filename.CompareTo("/WS_Finished_Reason_Codes") == 0)
                {

                    WS_Finished_Reason_Codes.WS_Finished_Reason_Codes[] newItemsInterface = new WS_Finished_Reason_Codes.WS_Finished_Reason_Codes[1];
                    WS_Finished_Reason_Codes_PortClient myItem = new WS_Finished_Reason_Codes_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Code", newItemsInterface[i].Code);
                            obj.Add("Description", newItemsInterface[i].Description);
                            obj.Add("Key", newItemsInterface[i].Key);
                            array.Add(obj);
                            //Console.WriteLine(newItemsInterface[i]. + " " + newItemsInterface[i].Field_Type /*+ " "+ newItemsInterface[i].Key*/);

                        }
                        //System.Threading.Thread.Sleep(100000);
                        //oJsonObject.Add(array);

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                if (filename.CompareTo("/WS_Terminal_Setup") == 0)
                {

                    WS_Terminal_Setup.WS_Terminal_Setup[] newItemsInterface = new WS_Terminal_Setup.WS_Terminal_Setup[1];
                    WS_Terminal_Setup_PortClient myItem = new WS_Terminal_Setup_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    try
                    {

                        newItemsInterface = myItem.ReadMultiple(null, null, 0);     //Leggi Tutti i record

                        JObject oJsonObject = new JObject();

                        Console.WriteLine("Letti " + newItemsInterface.GetLength(0).ToString() + " Articoli");

                        JArray array = new JArray();
                        for (int i = 0; i < newItemsInterface.GetLength(0); i++)
                        {

                            JObject obj = new JObject();
                            obj.Add("Component_Scrap_Report_No", newItemsInterface[i].Component_Scrap_Report_No);
                            obj.Add("Description", newItemsInterface[i].Description);
                            obj.Add("Gen_Prod_Posting_Group", newItemsInterface[i].Gen_Prod_Posting_Group);
                            obj.Add("Journal_Batch_Name", newItemsInterface[i].Journal_Batch_Name);
                            obj.Add("Journal_Template_Name", newItemsInterface[i].Journal_Template_Name);
                            obj.Add("Key", newItemsInterface[i].Key);
                            obj.Add("Name", newItemsInterface[i].Name);
                            obj.Add("Output_Report_No", newItemsInterface[i].Output_Report_No);
                            obj.Add("Output_Report_No_SL", newItemsInterface[i].Output_Report_No_SL);
                            obj.Add("Output_Time_Report_No_SL", newItemsInterface[i].Output_Time_Report_No_SL);
                            obj.Add("Password", newItemsInterface[i].Password);
                            obj.Add("Rework_Journal_Batch_Name", newItemsInterface[i].Rework_Journal_Batch_Name);
                            obj.Add("Rework_Journal_Template_Name", newItemsInterface[i].Rework_Journal_Template_Name);
                            obj.Add("Routing_Link_Code", newItemsInterface[i].Routing_Link_Code);
                            obj.Add("Scrap_Journal_Batch_Name", newItemsInterface[i].Scrap_Journal_Batch_Name);
                            obj.Add("Scrap_Journal_Template_Name", newItemsInterface[i].Scrap_Journal_Template_Name);
                            obj.Add("Teminal_Server", newItemsInterface[i].Teminal_Server);
                            obj.Add("To_Stapling", newItemsInterface[i].To_Stapling);
                            obj.Add("Work_Center_Customer_Lot_No", newItemsInterface[i].Work_Center_Customer_Lot_No);
                            obj.Add("Work_Center_No", newItemsInterface[i].Work_Center_No);
                            array.Add(obj);

                        }

                        Stream input;
                        using (input = GenerateStreamFromString(array.ToString()))
                        {

                            //Adding permanent http response headers
                            context.Response.ContentType = "application/json";
                            context.Response.ContentLength64 = input.Length;
                            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                            byte[] buffer = new byte[1024 * 16];
                            int nbytes;
                            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                context.Response.OutputStream.Write(buffer, 0, nbytes);
                            input.Close();
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        context.Response.OutputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errGET++;
                }

                if (errGET == 12)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }

            }
            if (type == "POST")
            {
                /******************* WS_Manufacturing_Functions/ReqSessionWork *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ReqSessioneWork") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string TerminalNo = results.TerminalNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string MachineCenterNo = results.MachineCenterNo;
                        int Addetti = results.Addetti;
                        string StaplingNo = results.StaplingNo;
                        string SessionType = results.SessionType;
                        bool Starting = results.Starting;
                        string TimeString = results.Time;


                        TimeString = TimeString.Replace('.', ':');

                        Console.WriteLine(TimeString);

                        DateTime Time = new DateTime();

                        Time = DateTime.ParseExact(TimeString, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        if (errore == 0)
                        {

                            Console.WriteLine(TerminalNo + "|" + WorkCenterNo + "|" + MachineCenterNo + "|" +
                                Addetti + "|" + StaplingNo + "|" + SessionType + "|" +
                                Starting + "|" + Time);

                            string RoutingNo = string.Empty;
                            string OperationNo = string.Empty;
                            string LotNo = string.Empty;
                            string ItemNo = string.Empty;
                            decimal[] ArrayQty = new decimal[5] { 1,2,3,4,5};
                            int ProductionSort = 0;
                            DateTime StartingDate = new DateTime();


                            //iTerminalNo,iWorkCenterNo,iMachineCenterNo,iAddetti,iStaplingNo,iSessionType,iStarting,iTime
                            //oRoutingNo,oOperationNo,oLotNo,oItemNo,oArrayQty

                            myItem.ReqSessioneWork(ref TerminalNo, ref WorkCenterNo
                                , ref MachineCenterNo, ref Addetti, ref StaplingNo
                                , ref SessionType, ref Starting, ref Time, ref RoutingNo,
                                ref OperationNo, ref LotNo, ref ItemNo, ref ArrayQty, ref ProductionSort, ref StartingDate);  //Leggi Tutti i record
                            
                            /*if (ArrayQty.Length > 0)
                            {
                                for (int i = 0; i < ArrayQty.Length; i++)
                                {

                                    Console.WriteLine(ArrayQty[i]);

                                }

                            }*/

                            decimal[] finalArray = new decimal[5] {
                                ArrayQty[0],
                                ArrayQty[2],
                                ArrayQty[4],
                                ArrayQty[6],
                                ArrayQty[8]
                            };
                            Console.WriteLine(RoutingNo + "|" + OperationNo + "|" + LotNo + "|" + ItemNo);

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("RoutingNo", RoutingNo);
                            oJsonObject.Add("OperationNo", OperationNo);
                            oJsonObject.Add("LotNo", LotNo);
                            oJsonObject.Add("ItemNo", ItemNo);
                            oJsonObject.Add("ProductionSort", ProductionSort);
                            oJsonObject.Add("StartingDate", StartingDate.ToString("dd/MM/yyyy HH:mm:ss"));

                            JArray array = new JArray();
                            if (finalArray.Length > 0)
                            {
                                for (int i = 0; i < finalArray.Length; i++)
                                {

                                    array.Add(finalArray[i]);

                                }
                                oJsonObject.Add("ArrayQty", array);
                            }

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    } catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/MgmtQuantity *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/MgmtQuantity") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        bool CalcTRacking = results.CalcTRacking;


                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + CalcTRacking);

                            List<string> a = new List<string>();
                            List<string> a1 = new List<string>();
                            List<string> a2 = new List<string>();
                            List<bool> a3 = new List<bool>();

                            List<decimal> b = new List<decimal>();
                            List<decimal> b1 = new List<decimal>();

                            List<string> c = new List<string>();
                            List<string> c1 = new List<string>();

                            List<decimal> d = new List<decimal>();

                            string[] ArrayCompCode = new string[30];

                            for (int i = 0; i < 30; i++)
                                ArrayCompCode[i] = "";

                            //string[] ArrayCompCode = a.ToArray();

                            string[] ArrayCompCodeDesc = new string[30];

                            for (int i = 0; i < 30; i++)
                                ArrayCompCodeDesc[i] = "";

                            //string[] ArrayCompCodeDesc = a1.ToArray();

                            string[] ArrayCompCodeUM = new string[30];

                            for (int i = 0; i < 30; i++)
                                ArrayCompCodeUM[i] = "";

                            //string[] ArrayCompCodeUM = a2.ToArray();

                            int[] ArrayCompCodeOK = new int[30];
                            //bool[] ArrayCompCodeOK = a3.ToArray();

                            decimal[] ArrayCompQtyTbP = new decimal[30];
                            //decimal[] ArrayCompQtyTbP = b.ToArray();

                            decimal[] ArrayCompQtyTbA = new decimal[30];
                            //decimal[] ArrayCompQtyTbA = b1.ToArray();

                            string[] ArrayLotNoItem = new string [30];

                            for (int i = 0; i < 30; i++)
                                ArrayLotNoItem[i] = "";

                            //string[] ArrayLotNoItem = c.ToArray();

                            string[] ArrayLotNoCode = new string[30];

                            for (int i = 0; i < 30; i++)
                                ArrayLotNoCode[i] = "";

                            //string[] ArrayLotNoCode = c1.ToArray();

                            decimal[] ArrayLotQty = new decimal[30];
                            //decimal[] ArrayLotQty = d.ToArray();




                            //iWorkCenterNo,iStaplingNo,iRoutingNo,iTerminalName,iCalcTRacking,quantityTotal


                            myItem.MgmtQuantity(ref RoutingNo, ref StaplingNo
                                , ref WorkCenterNo, ref TerminalName, ref CalcTRacking
                                , ref ArrayCompCode, ref ArrayCompCodeDesc, ref ArrayCompCodeUM
                                , ref ArrayCompCodeOK, ref ArrayCompQtyTbP, ref ArrayCompQtyTbA
                                , ref ArrayLotNoItem, ref ArrayLotNoCode, ref ArrayLotQty);
                            

                           /* myItem.MgmtQuantity(ref RoutingNo, ref StaplingNo
                                , ref WorkCenterNo, ref TerminalName, ref CalcTRacking
                                , ref ArrayCompQtyTbP, ref ArrayCompCodeDesc);*/

                            JObject oJsonObject = new JObject();

                            // GESTIONE ARRAY COMPCODE
                            JArray ArrayComp = new JArray();

                            JArray ArrayComp1 = new JArray();
                            JArray ArrayComp2 = new JArray();
                            JArray ArrayComp3 = new JArray();
                            JArray ArrayComp4 = new JArray();

                            for (int i = 0; i < ArrayCompCode.Length; i++)
                            {
                                ArrayComp1.Add(ArrayCompCode[i]);
                            }

                            for (int i = 0; i < ArrayCompCodeDesc.Length; i++)
                            {
                                ArrayComp2.Add(ArrayCompCodeDesc[i]);
                            }

                            for (int i = 0; i < ArrayCompCodeUM.Length; i++)
                            {
                                ArrayComp3.Add(ArrayCompCodeUM[i]);
                            }

                            for (int i = 0; i < ArrayCompCodeOK.Length; i++)
                            {
                                ArrayComp4.Add(ArrayCompCodeOK[i]);
                            }

                            ArrayComp.Add(ArrayComp1);
                            ArrayComp.Add(ArrayComp2);
                            ArrayComp.Add(ArrayComp3);
                            ArrayComp.Add( ArrayComp4);

                            oJsonObject.Add("ArrayCompCode", ArrayComp);

                            // GESTIONE ARRAY COMPQTY
                            JArray ArrayCompQty = new JArray();

                            JArray ArrayCompQty1 = new JArray();
                            JArray ArrayCompQty2 = new JArray();

                            for (int i = 0; i < ArrayCompQtyTbP.Length; i++)
                            {
                                if(i%2 == 0)
                                    ArrayCompQty1.Add(ArrayCompQtyTbP[i]);
                            }

                            for (int i = 0; i < ArrayCompQtyTbA.Length; i++)
                            {
                                if (i % 2 == 0)
                                    ArrayCompQty2.Add(ArrayCompQtyTbA[i]);
                            }

                            ArrayCompQty.Add(ArrayCompQty1);
                            ArrayCompQty.Add(ArrayCompQty2);

                            oJsonObject.Add("ArrayCompQty", ArrayCompQty);

                            // GESTIONE ARRAY LOTNO
                            JArray ArrayLotNo = new JArray();

                            JArray ArrayLotNo1 = new JArray();
                            JArray ArrayLotNo2 = new JArray();

                            for (int i = 0; i < ArrayLotNoItem.Length; i++)
                            {
                                ArrayLotNo1.Add(ArrayLotNoItem[i]);
                            }

                            for (int i = 0; i < ArrayLotNoCode.Length; i++)
                            {
                                ArrayLotNo2.Add(ArrayLotNoCode[i]);
                            }

                            ArrayLotNo.Add(ArrayLotNo1);
                            ArrayLotNo.Add(ArrayLotNo2);

                            oJsonObject.Add("ArrayLotNo", ArrayLotNo);


                            // GESTIONE ARRAY LOTQTY
                            JArray ArrayLotQt = new JArray();

                            JArray ArrayLotQty1 = new JArray();

                            for (int i = 0; i < ArrayLotQty.Length; i++)
                            {
                                if (i % 2 == 0)
                                    ArrayLotQty1.Add(ArrayLotQty[i]);

                            }

                            ArrayLotQt.Add(ArrayLotQty1);

                            oJsonObject.Add("ArrayLotQty", ArrayLotQt);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message +"|"+ex.Data.ToString()+"|"+ex.StackTrace+"");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/MgmtQuantityEnter *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/MgmtQuantityEnter") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        bool CalcTRacking = results.CalcTRacking;
                        decimal quantityTotal = results.quantityTotal;


                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + CalcTRacking + "|" + quantityTotal);


                            //iWorkCenterNo,iStaplingNo,iRoutingNo,iTerminalName,iCalcTRacking,quantityTotal

                            myItem.MgmtQuantityEnter(ref RoutingNo, ref StaplingNo
                                , ref WorkCenterNo, ref TerminalName, ref CalcTRacking
                                , quantityTotal);  //Leggi Tutti i record



                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/ExecuteOperation  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ExecuteOperation") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string TerminalNo = results.TerminalNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string MachineCenterNo = results.MachineCenterNo;
                        string RoutingNo = results.RoutingNo;
                        string OperationNo = results.OperationNo;
                        int AddettiNo = results.AddettiNo;
                        string StaplingNo = results.StaplingNo;
                        decimal Quantity = results.Quantity;
                        string TimeString = results.EndTime;


                        TimeString = TimeString.Replace('.', ':');

                        Console.WriteLine(TimeString);

                        DateTime EndTime = new DateTime();

                        EndTime = DateTime.ParseExact(TimeString, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        if (errore == 0)
                        {

                            Console.WriteLine(TerminalNo + "|" + WorkCenterNo + "|" + MachineCenterNo + "|" +
                                RoutingNo + "|" + OperationNo + "|" + AddettiNo + "|" +
                                StaplingNo + "|" + EndTime + "|" + Quantity);

                            bool PrintReport = false;
                            bool AskTruckData = false;

                            string RetText = myItem.ExecuteOperation(ref TerminalNo, ref WorkCenterNo
                                , ref MachineCenterNo, ref RoutingNo, ref OperationNo
                                , ref AddettiNo,ref StaplingNo, ref EndTime , ref Quantity
                                , ref PrintReport,ref AskTruckData);  //Leggi Tutti i record

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("RetText", RetText);
                            oJsonObject.Add("PrintReport", PrintReport);
                            oJsonObject.Add("AskTruckData", AskTruckData);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/ExecuteTimeOperation   *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ExecuteTimeOperation") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string TerminalNo = results.TerminalNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string MachineCenterNo = results.MachineCenterNo;
                        string RoutingNo = results.RoutingNo;
                        string OperationNo = results.OperationNo;
                        int AddettiNo = results.AddettiNo;
                        string StaplingNo = results.StaplingNo;
                        string TimeString = results.EndTime;


                        TimeString = TimeString.Replace('.', ':');

                        Console.WriteLine(TimeString);

                        DateTime EndTime = new DateTime();

                        EndTime = DateTime.ParseExact(TimeString, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        if (errore == 0)
                        {

                            Console.WriteLine(TerminalNo + "|" + WorkCenterNo + "|" + MachineCenterNo + "|" +
                                RoutingNo + "|" + OperationNo + "|" + AddettiNo + "|" +
                                StaplingNo + "|" + EndTime );

                            //string RoutingNo = string.Empty;
                            //string OperationNo = string.Empty;
                            //string LotNo = string.Empty;
                            //string ItemNo = string.Empty;
                            //decimal[] ArrayQty = new decimal[5] { 1, 2, 3, 4, 5 };


                            //iWorkCenterNo,iStaplingNo,iRoutingNo,iTerminalName,iCalcTRacking,quantityTotal
                            
                            myItem.ExecuteTimeOperation(ref TerminalNo, ref WorkCenterNo
                                , ref MachineCenterNo, ref RoutingNo, ref OperationNo
                                , ref AddettiNo, ref StaplingNo, ref EndTime);  //Leggi Tutti i record

                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();

                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/Reset   *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/Reset") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string TerminalNo = results.TerminalNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string MachineCenterNo = results.MachineCenterNo;
                        string RoutingNo = results.RoutingNo;
                        string OperationNo = results.OperationNo;
                        string ProdOrderNo = results.ProdOrderNo;

                        if (errore == 0)
                        {

                            Console.WriteLine(TerminalNo + "|" + WorkCenterNo + "|" + MachineCenterNo + "|" +
                                RoutingNo + "|" + OperationNo + "|" + ProdOrderNo);

                            //iWorkCenterNo,iStaplingNo,iRoutingNo,iTerminalName,iCalcTRacking,quantityTotal

                            myItem.Reset(ref TerminalNo, ref WorkCenterNo
                                , ref MachineCenterNo, ref RoutingNo, ref OperationNo
                                , ref ProdOrderNo);  //Leggi Tutti i record

                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();

                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }


                /******************* WS_Manufacturing_Functions/ScrapOnFinished *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ScrapOnFinished") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string MachineCenter = results.MachineCenter;
                        string ReasonCode = results.ReasonCode;
                        decimal QtyToBeScrap = results.QtyToBeScrap;
                        JArray ScrapReasonArray = results.ScrapReasonArray;
                        JArray ScrapQtyArray = results.ScrapQtyArray;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + MachineCenter + "|" + ReasonCode + "|" + QtyToBeScrap);

                            string[] iScrapReasonArray = new string[40];

                            for (int i = 0; i < 40; i++)
                                iScrapReasonArray[i] = "";

                            decimal[] iScrapQtyArray = new decimal[40];


                            int ii = 0;
                            foreach (string st in ScrapReasonArray) { 

                                iScrapReasonArray[ii] = st;

                                ii++;
                                if (ii == 40)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal de in ScrapQtyArray)
                            {

                                iScrapQtyArray[ii] = de;

                                ii++;
                                if (ii == 40)
                                    break;
                            }


                            //iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iMachineCenter,iReasonCode
                            //iQtyToBeScrap,iScrapReasonArray[string],iScrapQtyArray[decimal]
                            bool result = myItem.ScrapOnFinished(RoutingNo, StaplingNo, WorkCenterNo, TerminalName, MachineCenter,
                                ref ReasonCode, QtyToBeScrap, iScrapReasonArray, iScrapQtyArray);
                            

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("oReasonCode", ReasonCode);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/ScrapOnComponents *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ScrapOnComponents") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string UserName = results.UserName;

                        JArray ItemComponentNo = results.ItemComponentNo;
                        JArray ItemComponentDescription = results.ItemComponentDescription;
                        JArray ItemBaseUnitOfMeasure = results.ItemBaseUnitOfMeasure;

                        JArray BOMQuantity = results.BOMQuantity;
                        JArray QuantityToBeProduced = results.QuantityToBeProduced;
                        JArray ScrapQty = results.ScrapQty;

                        JArray ReasonCode = results.ReasonCode;
                        JArray Reason = results.Reason;

                        JArray CheckTrack = results.CheckTrack;
                        JArray CheckStock = results.CheckStock;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + ReadPost + "|" + UserName);

                            //****

                            string[] iItemComponentNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iItemComponentNo[i] = "";

                            string[] iItemComponentDescription = new string[100];

                            for (int i = 0; i < 100; i++)
                                iItemComponentDescription[i] = "";

                            string[] iItemBaseUnitOfMeasure = new string[100];

                            for (int i = 0; i < 100; i++)
                                iItemBaseUnitOfMeasure[i] = "";

                            //****

                            decimal[] iBOMQuantity = new decimal[100];
                            decimal[] iQuantityToBeProduced = new decimal[100];
                            decimal[] iScrapQty = new decimal[100];

                            //****

                            string[] iReasonCode = new string[100];

                            for (int i = 0; i < 100; i++)
                                iReasonCode[i] = "";

                            string[] iReason = new string[100];

                            for (int i = 0; i < 100; i++)
                                iReason[i] = "";

                            //****

                            int[] iCheckTrack = new int[100];
                            int[] iCheckStock = new int[100];

                            //****


                            int ii = 0;
                            if (ItemComponentNo != null)
                            {
                                foreach (string st in ItemComponentNo)
                                {

                                    iItemComponentNo[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;

                                }
                            }

                            if (ItemComponentDescription != null)
                            {
                                ii = 0;
                                foreach (string st in ItemComponentDescription)
                                {

                                    iItemComponentDescription[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ItemBaseUnitOfMeasure != null)
                            {
                                ii = 0;
                                foreach (string st in ItemBaseUnitOfMeasure)
                                {

                                    iItemBaseUnitOfMeasure[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (BOMQuantity != null)
                            {
                                ii = 0;
                                foreach (decimal st in BOMQuantity)
                                {

                                    iBOMQuantity[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (QuantityToBeProduced != null)
                            {
                                ii = 0;
                                foreach (decimal st in QuantityToBeProduced)
                                {
                                    if (ii % 2 == 0)
                                        iQuantityToBeProduced[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ScrapQty != null)
                            {
                                ii = 0;
                                foreach (decimal st in ScrapQty)
                                {

                                    iScrapQty[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ReasonCode != null)
                            {
                                ii = 0;
                                foreach (string st in ReasonCode)
                                {

                                    iReasonCode[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (Reason != null)
                            {
                                ii = 0;
                                foreach (string st in Reason)
                                {

                                    iReason[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (CheckTrack != null)
                            {
                                ii = 0;
                                foreach (int st in CheckTrack)
                                {

                                    iCheckTrack[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (CheckStock != null)
                            {
                                ii = 0;
                                foreach (int st in CheckStock)
                                {

                                    iCheckStock[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }
                            //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iUserName
                            //iItemComponentNo[string],iItemComponentDescription[string],iItemBaseUnitOfMeasure[string]
                            //iBOMQuantity[decimal],iQuantityToBeProduced[decimal],iScrapQty[decimal]
                            //iReasonCode[string],iReason[string]
                            //iCheckTrack[int],iCheckStock[int]

                            string result = "";


                            result = myItem.ScrapOnComponents(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName, UserName,
                                    ref iItemComponentNo, ref iItemComponentDescription, ref iItemBaseUnitOfMeasure,
                                    ref iBOMQuantity, ref iQuantityToBeProduced, ref iScrapQty,
                                    ref iReasonCode, ref iReason,
                                    ref iCheckTrack, ref iCheckStock);


                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            // STRING 

                            JArray oItemComponentNo = new JArray();
                            foreach (string v in iItemComponentNo)
                            {
                                oItemComponentNo.Add(v);
                            }
                            oJsonObject.Add("oItemComponentNo", oItemComponentNo);

                            JArray oItemComponentDescription = new JArray();
                            foreach (string v in iItemComponentDescription)
                            {
                                oItemComponentDescription.Add(v);
                            }
                            oJsonObject.Add("oItemComponentDescription", oItemComponentDescription);

                            JArray oItemBaseUnitOfMeasure = new JArray();
                            foreach (string v in iItemBaseUnitOfMeasure)
                            {
                                oItemBaseUnitOfMeasure.Add(v);
                            }
                            oJsonObject.Add("oItemBaseUnitOfMeasure", oItemBaseUnitOfMeasure);

                            // DECIMAL

                            JArray oBOMQuantity = new JArray();
                            int indice = 0;
                            foreach (decimal v in iBOMQuantity)
                            {
                                if (indice % 2 == 0)
                                    oBOMQuantity.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oBOMQuantity", oBOMQuantity);

                            JArray oQuantityToBeProduced = new JArray();
                            indice = 0;
                            foreach (decimal v in iQuantityToBeProduced)
                            {
                                if (indice % 2 == 0)
                                    oQuantityToBeProduced.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oQuantityToBeProduced", oQuantityToBeProduced);

                            JArray oScrapQty = new JArray();
                            indice = 0;
                            foreach (decimal v in iScrapQty)
                            {
                                if (indice % 2 == 0)
                                    oScrapQty.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oScrapQty", oScrapQty);

                            // STRING  

                            JArray oReasonCode = new JArray();
                            foreach (string v in iReasonCode)
                            {
                                oReasonCode.Add(v);
                            }
                            oJsonObject.Add("oReasonCode", oReasonCode);

                            JArray oReason = new JArray();
                            foreach (string v in iReason)
                            {
                                oReason.Add(v);
                            }
                            oJsonObject.Add("oReason", oReason);

                            // INT

                            JArray oCheckTrack = new JArray();
                            foreach (int v in iCheckTrack)
                            {
                                oCheckTrack.Add(v);
                            }
                            oJsonObject.Add("oCheckTrack", oCheckTrack);

                            JArray oCheckStock = new JArray();
                            foreach (int v in iCheckStock)
                            {
                                oCheckStock.Add(v);
                            }
                            oJsonObject.Add("oCheckStock", oCheckStock);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();
                            oJsonObject.Add("ReadPost", ReadPost);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                        
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/SetComponentLotToScrap *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/SetComponentLotToScrap") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iUserQuantity,iItemNo,iVariantCode,iBaseUnitOfMeasure
                        //iLotNo[string],iDescriptionLot[string]
                        //iLotQty[decimal]
                        //iExpirationDate[DateTime],iReceiptDate[DateTime]
                        //iInEntryNo[int]

                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        decimal UserQuantity = results.UserQuantity;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;

                        JArray LotNo = results.LotNo;
                        JArray DescriptionLot = results.DescriptionLot;

                        JArray LotQty = results.LotQty;

                        JArray ExpirationDate = results.ExpirationDate;
                        JArray ReceiptDate = results.ReceiptDate;

                        JArray InEntryNo = results.InEntryNo;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + ReadPost + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);

                            //****

                            string[] iLotNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iLotNo[i] = "";

                            string[] iDescriptionLot = new string[100];

                            for (int i = 0; i < 100; i++)
                                iDescriptionLot[i] = "";

                            //****

                            decimal[] iLotQty = new decimal[100];

                            //****

                            DateTime[] iExpirationDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iExpirationDate[i] = FromUnixTime(0);

                            DateTime[] iReceiptDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iReceiptDate[i] = FromUnixTime(0);

                            //****

                            int[] iInEntryNo = new int[100];

                            //****

                            int ii = 0;
                            foreach (string st in LotNo)
                            {

                                iLotNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in DescriptionLot)
                            {

                                iDescriptionLot[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (int st in LotQty)
                            {

                                iLotQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ExpirationDate)
                            {

                                iExpirationDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ReceiptDate)
                            {

                                iReceiptDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (int st in InEntryNo)
                            {

                                iInEntryNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iUserQuantity,iItemNo,iVariantCode,iBaseUnitOfMeasure
                            //iLotNo[string],iDescriptionLot[string]
                            //iLotQty[decimal]
                            //iExpirationDate[DateTime],iReceiptDate[DateTime]
                            //iInEntryNo[int]

                            bool result = false;
                       
                            result = myItem.SetComponentLotToScrap(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName, UserQuantity, ItemNo, VariantCode, BaseUnitOfMeasure,
                                ref iLotNo, ref iDescriptionLot,
                                ref iLotQty,
                                ref iExpirationDate, ref iReceiptDate,
                                ref iInEntryNo);

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            
                       
                            JArray oLotNo = new JArray();
                            foreach (string v in iLotNo)
                            {
                                    oLotNo.Add(v);
                            }
                            oJsonObject.Add("oLotNo", oLotNo);

                            JArray oDescriptionLot = new JArray();
                            foreach (string v in iDescriptionLot)
                            {
                                oDescriptionLot.Add(v);
                            }
                            oJsonObject.Add("oDescriptionLot", oDescriptionLot);

                            JArray oLotQty = new JArray();
                            int indice = 0;
                            foreach (decimal v in iLotQty)
                            {
                                if (indice % 2 == 0)
                                    oLotQty.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oLotQty", oLotQty);
                            

                            JArray oExpirationDate = new JArray();
                            foreach (DateTime v in iExpirationDate)
                            {
                                oExpirationDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oExpirationDate", oExpirationDate);

                            JArray oReceiptDate = new JArray();
                            foreach (DateTime v in iReceiptDate)
                            {
                                oReceiptDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oReceiptDate", oReceiptDate);

                            JArray oInEntryNo = new JArray();
                            foreach (int v in iInEntryNo)
                            {
                                oInEntryNo.Add(v);
                            }
                            oJsonObject.Add("oInEntryNo", oInEntryNo);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();
                            oJsonObject.Add("ReadPost", ReadPost);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/PrepareLotComponentToScrap *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PrepareLotComponentToScrap") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;

                        string LocationCode = results.LocationCode;
                        string BinCode = results.BinCode;

                        decimal UserQuantity = results.UserQuantity;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + ItemNo + "|" + VariantCode + "|" +
                                BaseUnitOfMeasure + "|" + LocationCode + "|" + BinCode + "|" +
                                UserQuantity);


                            //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iItemNo,iVariantCode,iBaseUnitOfMeasure ,
                            //iLocationCode ,iBinCode ,iUserQuantity

                            bool result = false;

                            result = myItem.PrepareComponentLotToScrap( RoutingNo, StaplingNo, WorkCenterNo, TerminalName, ItemNo, VariantCode, BaseUnitOfMeasure,
                                ref LocationCode, ref BinCode, UserQuantity);

                            

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("LocationCode", LocationCode);
                            oJsonObject.Add("BinCode", BinCode);
                            oJsonObject.Add("UserQuantity", UserQuantity);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/SelectedStapling  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/SelectedStapling") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        string RoutingProdNo = results.RoutingProdNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ProdOrderNo = results.ProdOrderNo;
                        string OperationNo = results.OperationNo;

                        string StartDateTS = results.StartDateTime;

                        StartDateTS = StartDateTS.Replace('.', ':');

                        Console.WriteLine(StartDateTS);

                        DateTime StartDate = new DateTime();

                        StartDate = DateTime.ParseExact(StartDateTS, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        //** VALORI IN USCITA **
                        string StaplingLotNo = "";
                        string ItemNo = "";

                        decimal ProducedQty = 0;
                        decimal ReworkedQty = 0;
                        decimal ScrapQty = 0;
                        decimal QtyToProduce = 0;
                        //********************

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingProdNo + "|" + StaplingNo + "|" + WorkCenterNo + "|" +
                                TerminalName + "|" + ProdOrderNo + "|" + OperationNo + "|" +
                                StaplingLotNo + "|" + ItemNo + "|" + ProducedQty + "|" +
                                ReworkedQty + "|" + ScrapQty + "|" + QtyToProduce);


                            //iRoutingProdNo, iStaplingNo, iWorkCenterNo, iTerminalName, iProdOrderNo, iOperationNo, iStaplingLotNo, iItemNo
                            //iProducedQty ,iReworkedQty, iScrapQty, iQtyToProduce,
                            //iStartDate, iStartTime

                            bool result = false;

                            result = myItem.SelectedStapling(RoutingProdNo, StaplingNo, WorkCenterNo, TerminalName,ref ProdOrderNo,ref OperationNo,
                                ref StaplingLotNo,ref ItemNo, ref ProducedQty,ref ReworkedQty, ref ScrapQty, ref QtyToProduce, StartDate);



                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("StaplingLotNo", StaplingLotNo);
                            oJsonObject.Add("ItemNo", ItemNo);
                            oJsonObject.Add("ProducedQty", ProducedQty);
                            oJsonObject.Add("ReworkedQty", ReworkedQty);
                            oJsonObject.Add("ScrapQty", ScrapQty);
                            oJsonObject.Add("QtyToProduce", QtyToProduce);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }


                /******************* WS_Manufacturing_Functions/SetComponentLotToStock  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/SetComponentLotToStock") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        
                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        decimal UserQuantity = results.UserQuantity;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;


                        JArray LotNo = results.LotNo;
                        JArray DescriptionLot = results.DescriptionLot;
                        JArray LotQty = results.LotQty;
                        JArray ExpirationDate = results.ExpirationDate;
                        JArray ReceiptDate = results.ReceiptDate;
                        JArray InEntryNo = results.InEntryNo;

                        if (errore == 0)
                        {

                            Console.WriteLine(ReadPost + "|" + RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);

                            //****

                            string[] iLotNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iLotNo[i] = "";

                            string[] iDescriptionLot = new string[100];

                            for (int i = 0; i < 100; i++)
                                iDescriptionLot[i] = "";

                            //****

                            decimal[] iLotQty = new decimal[100];

                            //****

                            DateTime[] iExpirationDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iExpirationDate[i] = FromUnixTime(0);

                            DateTime[] iReceiptDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iReceiptDate[i] = FromUnixTime(0);

                            //****

                            int[] iInEntryNo = new int[100];

                            //****

                            int ii = 0;
                            foreach (string st in LotNo)
                            {

                                iLotNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in DescriptionLot)
                            {

                                iDescriptionLot[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (int st in LotQty)
                            {

                                iLotQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ExpirationDate)
                            {

                                iExpirationDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ReceiptDate)
                            {

                                iReceiptDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (int st in InEntryNo)
                            {

                                iInEntryNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }


                            bool result = false;

                            //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iUserQuantity, iItemNo , iVariantCode
                            //iBaseUnitOfMeasure, iLotNo[], iDescriptionLot[], iLotQty[]
                            //iExpirationDate[], iReceiptDate[], iInEntryNo[]

                            result = myItem.SetComponentLotToStock(ref ReadPost,RoutingNo,StaplingNo,WorkCenterNo,TerminalName,UserQuantity,ItemNo,
                                VariantCode,BaseUnitOfMeasure,ref iLotNo,ref iDescriptionLot,ref iLotQty,iExpirationDate,iReceiptDate,ref iInEntryNo);



                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            JArray oLotNo = new JArray();
                            foreach (string v in iLotNo)
                            {
                                oLotNo.Add(v);
                            }
                            oJsonObject.Add("oLotNo", oLotNo);

                            JArray oDescriptionLot = new JArray();
                            foreach (string v in iDescriptionLot)
                            {
                                oDescriptionLot.Add(v);
                            }
                            oJsonObject.Add("oDescriptionLot", oDescriptionLot);

                            JArray oLotQty = new JArray();
                            int indice = 0;
                            foreach (decimal v in iLotQty)
                            {
                                if (indice % 2 == 0)
                                    oLotQty.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oLotQty", oLotQty);

                            JArray oExpirationDate = new JArray();
                            foreach (DateTime v in iExpirationDate)
                            {
                                oExpirationDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oExpirationDate", oExpirationDate);

                            JArray oReceiptDate = new JArray();
                            foreach (DateTime v in iReceiptDate)
                            {
                                oReceiptDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oReceiptDate", oReceiptDate);

                            JArray oInEntryNo = new JArray();
                            foreach (int v in iInEntryNo)
                            {
                                oInEntryNo.Add(v);
                            }
                            oJsonObject.Add("oInEntryNo", oInEntryNo);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

               

                /******************* WS_Manufacturing_Functions/PrepareLotComponentToStock  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PrepareLotComponentToStock") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;
                        string LocationCode = results.LocationCode;
                        string BinCode = results.BinCode;

                        decimal UserQuantity = results.UserQuantity;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);

                           


                            bool result = false;

                            //iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iItemNo , iVariantCode
                            //iBaseUnitOfMeasure, iLocationCode, iBinCode, iUserQuantity

                            result = myItem.PrepareComponentLotToStock(RoutingNo, StaplingNo, WorkCenterNo, TerminalName, ItemNo,
                                VariantCode, BaseUnitOfMeasure, ref LocationCode, ref BinCode, UserQuantity);

                            Console.WriteLine(1);

                            JObject oJsonObject = new JObject();
                            Console.WriteLine(1);
                            oJsonObject.Add("result", result);
                            oJsonObject.Add("LocationCode", LocationCode);
                            oJsonObject.Add("BinCode", BinCode);
                            oJsonObject.Add("UserQuantity", UserQuantity);

                            Console.WriteLine(1);
                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }


                /******************* WS_Manufacturing_Functions/Reworking  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/Reworking") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName
                        //iPriority[]
                        //iProdOrderNo[]
                        //iOrderQty[], iProducedQty[], iReworkedQty[], iQuantityToAdvance[], iQuantityToBeReworked[]
                        //iForcedQty[]
                        //iTotalReworkedQty[],iTotalQtyToRework

                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;

                        JArray Priority = results.Priority;

                        JArray ProdOrderNo = results.ProdOrderNo;

                        JArray OrderQty = results.OrderQty;
                        JArray ProducedQty = results.ProducedQty;
                        JArray ReworkedQty = results.ReworkedQty;
                        JArray QuantityToAdvance = results.QuantityToAdvance;
                        JArray QuantityToBeReworked = results.QuantityToBeReworked;

                        JArray ForcedQty = results.ForcedQty;

                        decimal TotalReworkedQty = results.TotalReworkedQty;
                        decimal TotalQtyToRework = results.TotalQtyToRework;

                        if (errore == 0)
                        {

                            Console.WriteLine(ReadPost + "|" + RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName);


                            //****

                            int[] iPriority = new int[100];

                            //***

                            string[] iProdOrderNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iProdOrderNo[i] = "";

                            //****

                            decimal[] iOrderQty = new decimal[100];

                            decimal[] iProducedQty = new decimal[100];

                            decimal[] iReworkedQty = new decimal[100];

                            decimal[] iQuantityToAdvance = new decimal[100];

                            decimal[] iQuantityToBeReworked = new decimal[100];

                            //****

                            bool[] iForcedQty = new bool[100];

                            for (int i = 0; i < 100; i++)
                                iForcedQty[i] = false;

                            //***

                            int ii = 0;
                            foreach (int st in Priority)
                            {

                                iPriority[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ProdOrderNo)
                            {

                                iProdOrderNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in OrderQty)
                            {

                                iOrderQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in ProducedQty)
                            {

                                iProducedQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in ReworkedQty)
                            {

                                iReworkedQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in QuantityToAdvance)
                            {

                                iQuantityToAdvance[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in QuantityToBeReworked)
                            {

                                iQuantityToBeReworked[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (bool st in ForcedQty)
                            {

                                iForcedQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }


                            bool result = false;

                            //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName
                            //iPriority[]
                            //iProdOrderNo[]
                            //iOrderQty[], iProducedQty[], iReworkedQty[], iQuantityToAdvance[], iQuantityToBeReworked[]
                            //iForcedQty[]
                            //iTotalReworkedQty[],iTotalQtyToRework

                            result = myItem.Reworking(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName,
                                ref iPriority, ref iProdOrderNo, ref iOrderQty, ref iProducedQty, ref iReworkedQty, ref iQuantityToAdvance,
                                ref iQuantityToBeReworked, ref iForcedQty, ref TotalReworkedQty, ref TotalQtyToRework);




                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            JArray ArrPriority = new JArray();
                            foreach (int s in iPriority)
                            {
                                ArrPriority.Add(s);
                            }
                            oJsonObject.Add("Priority", ArrPriority);

                            JArray ArrProdOrderNo = new JArray();
                            foreach (string s in iProdOrderNo)
                            {
                                ArrProdOrderNo.Add(s);
                            }
                            oJsonObject.Add("ProdOrderNo", ArrProdOrderNo);

                            JArray ArrOrderQty = new JArray();
                            foreach (decimal s in iOrderQty)
                            {
                                ArrOrderQty.Add(s);
                            }
                            oJsonObject.Add("OrderQty", ArrOrderQty);

                            JArray ArrProducedQty  = new JArray();
                            foreach (decimal s in iProducedQty)
                            {
                                ArrProducedQty.Add(s);
                            }
                            oJsonObject.Add("ProducedQty", ArrProducedQty);

                            JArray ArrReworkedQty = new JArray();
                            foreach (decimal s in iReworkedQty)
                            {
                                ArrReworkedQty.Add(s);
                            }
                            oJsonObject.Add("ReworkedQty", ArrReworkedQty);

                            JArray ArrQuantityToAdvance = new JArray();
                            foreach (decimal s in iQuantityToAdvance)
                            {
                                ArrQuantityToAdvance.Add(s);
                            }
                            oJsonObject.Add("QuantityToAdvance", ArrQuantityToAdvance);

                            JArray ArrQuantityToBeReworked = new JArray();
                            foreach (decimal s in iQuantityToBeReworked)
                            {
                                ArrQuantityToBeReworked.Add(s);
                            }
                            oJsonObject.Add("QuantityToBeReworked", ArrQuantityToBeReworked);

                            JArray ArrForcedQty = new JArray();
                            foreach (bool s in iForcedQty)
                            {
                                ArrForcedQty.Add(s);
                            }
                            oJsonObject.Add("ForcedQty", ArrForcedQty);

                            oJsonObject.Add("TotalReworkedQty", TotalReworkedQty);
                            oJsonObject.Add("TotalQtyToRework", TotalQtyToRework);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }



                /******************* WS_Manufacturing_Functions/PostRework  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PostRework") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ReasonCode = results.ReasonCode;
                        string MachineCenter = results.MachineCenter;
                        decimal QtyToRework = results.QtyToRework;

                        JArray ScrapReasonArray = results.ScrapReasonArray;

                        JArray ScrapQtyArray = results.ScrapQtyArray;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName+ "|"+ReasonCode);


                            //***

                            string[] iScrapReasonArray = new string[100];

                            for (int i = 0; i < 100; i++)
                                iScrapReasonArray[i] = "";

                            //****

                            decimal[] iScrapQtyArray = new decimal[100];

                            //****


                            int ii = 0;
                            foreach (string st in ScrapReasonArray)
                            {

                                iScrapReasonArray[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in ScrapQtyArray)
                            {

                                iScrapQtyArray[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }


                            string result = "";

                            //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iReasonCode
                            //iScrapReasonArray[]
                            //iScrapQtyArray[]

                            result = myItem.PostRework(RoutingNo, StaplingNo, WorkCenterNo, TerminalName,
                                ReasonCode, iScrapReasonArray, iScrapQtyArray, MachineCenter, QtyToRework);




                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);

                            JArray oScrapReasonArray = new JArray();
                            foreach (string s in iScrapReasonArray)
                            {
                                oScrapReasonArray.Add(s);
                            }
                            oJsonObject.Add("oScrapReasonArray", oScrapReasonArray);

                            JArray oScrapQtyArray = new JArray();
                            foreach (decimal s in iScrapQtyArray)
                            {
                                oScrapQtyArray.Add(s);
                            }
                            oJsonObject.Add("oScrapQtyArray", oScrapQtyArray);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }



                /******************* WS_Manufacturing_Functions/ReworkingComponents  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ReworkingComponents") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);

                        //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName
                        //iTrackingState[]
                        //iItemNo[], iItemDescription[], iBaseUnitOfMeasure[]
                        //iBOMQuantity[], iTotalQty[], iQtyToRework[]

                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;

                        JArray TrackingState = results.TrackingState;

                        JArray ItemNo = results.ItemNo;
                        JArray ItemDescription = results.ItemDescription;
                        JArray BaseUnitOfMeasure = results.BaseUnitOfMeasure;

                        JArray BOMQuantity = results.BOMQuantity;
                        JArray TotalQty = results.TotalQty;
                        JArray QtyToRework = results.QtyToRework;

                        if (errore == 0)
                        {

                            Console.WriteLine(ReadPost + "|" + RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName);


                            //****

                            int[] iTrackingState = new int[100];

                            //***

                            string[] iItemNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iItemNo[i] = "";

                            string[] iItemDescription = new string[100];

                            for (int i = 0; i < 100; i++)
                                iItemDescription[i] = "";

                            string[] iBaseUnitOfMeasure = new string[100];

                            for (int i = 0; i < 100; i++)
                                iBaseUnitOfMeasure[i] = "";

                            //****

                            decimal[] iBOMQuantity = new decimal[100];

                            decimal[] iTotalQty = new decimal[100];

                            decimal[] iQtyToRework = new decimal[100];

                            //****



                            int ii = 0;
                            foreach (int st in TrackingState)
                            {

                                iTrackingState[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ItemNo)
                            {

                                iItemNo[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in ItemDescription)
                            {

                                iItemDescription[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in BaseUnitOfMeasure)
                            {

                                iBaseUnitOfMeasure[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in BOMQuantity)
                            {
                                if (ii % 2 == 0)
                                    iBOMQuantity[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in TotalQty)
                            {
                                if(ii%2 == 0)
                                    iTotalQty[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (decimal st in QtyToRework)
                            {

                                iQtyToRework[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }


                            bool result = false;

                            //iReadPost, iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName
                            //iTrackingState[]
                            //iItemNo[], iItemDescription[], iBaseUnitOfMeasure[]
                            //iBOMQuantity[], iTotalQty[], iQtyToRework[]

                            result = myItem.ReworkingComponents(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName,
                                ref iTrackingState, ref iItemNo, ref iItemDescription, ref iBaseUnitOfMeasure,
                                ref iBOMQuantity, ref iTotalQty, ref iQtyToRework);




                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            JArray ArrTrackingState = new JArray();
                            foreach (int s in iTrackingState)
                            {
                                ArrTrackingState.Add(s);
                            }
                            oJsonObject.Add("TrackingState", ArrTrackingState);

                            JArray ArrItemNo = new JArray();
                            foreach (string s in iItemNo)
                            {
                                ArrItemNo.Add(s);
                            }
                            oJsonObject.Add("ItemNo", ArrItemNo);

                            JArray ArrItemDescription = new JArray();
                            foreach (string s in iItemDescription)
                            {
                                ArrItemDescription.Add(s);
                            }
                            oJsonObject.Add("ItemDescription", ArrItemDescription);

                            JArray ArrBaseUnitOfMeasure = new JArray();
                            foreach (string s in iBaseUnitOfMeasure)
                            {
                                ArrBaseUnitOfMeasure.Add(s);
                            }
                            oJsonObject.Add("BaseUnitOfMeasure", ArrBaseUnitOfMeasure);

                            JArray ArrBOMQuantity = new JArray();
                            for (int i = 0; i < iBOMQuantity.Length; i++)
                            {
                                if (i % 2 == 0)
                                    ArrBOMQuantity.Add(iBOMQuantity[i]);

                            }
                            oJsonObject.Add("BOMQuantity", ArrBOMQuantity);

                            JArray ArrTotalQty = new JArray();
                            for (int i = 0; i < iTotalQty.Length; i++)
                            {
                                if (i % 2 == 0)
                                    ArrTotalQty.Add(iTotalQty[i]);

                            }
                            oJsonObject.Add("TotalQty", ArrTotalQty);

                            JArray ArrQtyToRework = new JArray();
                            for (int i = 0; i < iQtyToRework.Length; i++)
                            {
                                if (i % 2 == 0)
                                    ArrQtyToRework.Add(iQtyToRework[i]);

                            }
                            oJsonObject.Add("QtyToRework", ArrQtyToRework);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/SetComponentLotToRework  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/SetComponentLotToRework") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);


                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        decimal UserQuantity = results.UserQuantity;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;


                        JArray LotNo = results.LotNo;
                        JArray DescriptionLot = results.DescriptionLot;
                        JArray LotQty = results.LotQty;
                        JArray ExpirationDate = results.ExpirationDate;
                        JArray ReceiptDate = results.ReceiptDate;
                        JArray InEntryNo = results.InEntryNo;

                        if (errore == 0)
                        {

                            Console.WriteLine(ReadPost + "|" + RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);

                            //****

                            string[] iLotNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iLotNo[i] = "";

                            string[] iDescriptionLot = new string[100];

                            for (int i = 0; i < 100; i++)
                                iDescriptionLot[i] = "";

                            //****

                            decimal[] iLotQty = new decimal[100];

                            //****

                            DateTime[] iExpirationDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iExpirationDate[i] = FromUnixTime(0);

                            DateTime[] iReceiptDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iReceiptDate[i] = FromUnixTime(0);

                            //****

                            int[] iInEntryNo = new int[100];

                            //****
                            int ii = 0;

                            if (LotNo != null)
                            {
                                foreach (string st in LotNo)
                                {

                                    iLotNo[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (DescriptionLot != null)
                            {
                                ii = 0;
                                foreach (string st in DescriptionLot)
                                {

                                    iDescriptionLot[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (LotQty != null)
                            {
                                ii = 0;
                                foreach (int st in LotQty)
                                {

                                    iLotQty[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ExpirationDate != null)
                            {
                                ii = 0;
                                foreach (string st in ExpirationDate)
                                {

                                    iExpirationDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ReceiptDate != null)
                            {
                                ii = 0;
                                foreach (string st in ReceiptDate)
                                {

                                    iReceiptDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (InEntryNo != null)
                            {
                                ii = 0;
                                foreach (int st in InEntryNo)
                                {

                                    iInEntryNo[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                               
                            }

                            bool result = false;

                            //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iUserQuantity,iItemNo,iVariantCode,iBaseUnitOfMeasure
                            //iLotNo[string],iDescriptionLot[string]
                            //iLotQty[decimal]
                            //iExpirationDate[DateTime],iReceiptDate[DateTime]
                            //iInEntryNo[int]


                            result = myItem.SetComponentLotToRework(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName, UserQuantity, ItemNo,
                                VariantCode, BaseUnitOfMeasure, ref iLotNo, ref iDescriptionLot, ref iLotQty, iExpirationDate, iReceiptDate, ref iInEntryNo);





                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            JArray oLotNo = new JArray();
                            foreach (string v in iLotNo)
                            {
                                oLotNo.Add(v);
                            }
                            oJsonObject.Add("oLotNo", oLotNo);

                            JArray oDescriptionLot = new JArray();
                            foreach (string v in iDescriptionLot)
                            {
                                oDescriptionLot.Add(v);
                            }
                            oJsonObject.Add("oDescriptionLot", oDescriptionLot);

                            JArray oLotQty = new JArray();
                            foreach (decimal v in iLotQty)
                            {
                                oLotQty.Add(v);
                            }
                            oJsonObject.Add("oLotQty", oLotQty);

                            JArray oExpirationDate = new JArray();
                            foreach (DateTime v in iExpirationDate)
                            {
                                oExpirationDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oExpirationDate", oExpirationDate);

                            JArray oReceiptDate = new JArray();
                            foreach (DateTime v in iReceiptDate)
                            {
                                oReceiptDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oReceiptDate", oReceiptDate);

                            JArray oInEntryNo = new JArray();
                            foreach (int v in iInEntryNo)
                            {
                                oInEntryNo.Add(v);
                            }
                            oJsonObject.Add("oInEntryNo", oInEntryNo);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }



                /******************* WS_Manufacturing_Functions/PrepareLotComponentToRework  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PrepareLotComponentToRework") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);


                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;
                        string LocationCode = results.LocationCode;
                        string BinCode = results.BinCode;

                        decimal UserQuantity = results.UserQuantity;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);




                            bool result = false;

                            //iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iItemNo , iVariantCode
                            //iBaseUnitOfMeasure, iLocationCode, iBinCode, iUserQuantity

                            result = myItem.PrepareComponentLotToRework(RoutingNo, StaplingNo, WorkCenterNo, TerminalName, ItemNo,
                                VariantCode, BaseUnitOfMeasure, ref LocationCode, ref BinCode, UserQuantity);



                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("LocationCode", LocationCode);
                            oJsonObject.Add("BinCode", BinCode);
                            oJsonObject.Add("UserQuantity", UserQuantity);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }


                /******************* WS_Manufacturing_Functions/ScrapReasonCodes  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/ScrapReasonCodes") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);


                        string WorkCenterNo = results.WorkCenterNo;

                        JArray Code = results.Code;
                        JArray Description = results.Description;

                        if (errore == 0)
                        {

                            Console.WriteLine(WorkCenterNo);

                            //****

                            string[] iCode = new string[100];

                            for (int i = 0; i < 100; i++)
                                iCode[i] = "";

                            string[] iDescription = new string[100];

                            for (int i = 0; i < 100; i++)
                                iDescription[i] = "";

                            //****

                            int ii = 0;
                            foreach (string st in Code)
                            {

                                iCode[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }

                            ii = 0;
                            foreach (string st in Description)
                            {

                                iDescription[ii] = st;

                                ii++;
                                if (ii == 100)
                                    break;
                            }
                            
                            string result = "";

                            //iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iItemNo , iVariantCode
                            //iBaseUnitOfMeasure, iLocationCode, iBinCode, iUserQuantity

                            result = myItem.ScrapReasonCodes(WorkCenterNo, ref iCode, ref iDescription);

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);

                            JArray oCode = new JArray();
                            foreach (string v in iCode)
                            {
                                oCode.Add(v);
                            }
                            oJsonObject.Add("LotNo", oCode);

                            JArray oDescription = new JArray();
                            foreach (string v in iDescription)
                            {
                                oDescription.Add(v);
                            }
                            oJsonObject.Add("Description", oDescription);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/PrintReport  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PrintReport") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);
                        string TerminalNo = results.TerminalNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string MachineCenterNo = results.MachineCenterNo;
                        string StaplingNo = results.StaplingNo;
                        string RoutingNo = results.RoutingNo;
                        string OperationNo = results.OperationNo;
                        decimal TruckQty = results.TruckQty;
                        int NoTrucks = results.NoTrucks;

                        if (errore == 0)
                        {

                            Console.WriteLine(TerminalNo + "|" + WorkCenterNo + "|" + MachineCenterNo + "|" +
                                RoutingNo + "|" + OperationNo + "|" + TruckQty + "|" +
                                StaplingNo + "|" + NoTrucks);

                            string pTxFilePDF = myItem.PrintReport(TerminalNo, WorkCenterNo
                                , MachineCenterNo, StaplingNo, RoutingNo, OperationNo
                                , TruckQty, NoTrucks);  //Leggi Tutti i record

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("pTxFilePDF", pTxFilePDF);

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/SetComponentLotToHandle  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/SetComponentLotToHandle") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);


                        int ReadPost = results.ReadPost;
                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        decimal UserQuantity = results.UserQuantity;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;


                        JArray LotNo = results.LotNo;
                        JArray DescriptionLot = results.DescriptionLot;
                        JArray LotQty = results.LotQty;
                        JArray ExpirationDate = results.ExpirationDate;
                        JArray ReceiptDate = results.ReceiptDate;
                        JArray InEntryNo = results.InEntryNo;

                        if (errore == 0)
                        {

                            Console.WriteLine(ReadPost + "|" + RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);

                            //****

                            string[] iLotNo = new string[100];

                            for (int i = 0; i < 100; i++)
                                iLotNo[i] = "";

                            string[] iDescriptionLot = new string[100];

                            for (int i = 0; i < 100; i++)
                                iDescriptionLot[i] = "";

                            //****

                            decimal[] iLotQty = new decimal[100];

                            //****

                            DateTime[] iExpirationDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iExpirationDate[i] = FromUnixTime(0);

                            DateTime[] iReceiptDate = new DateTime[100];

                            for (int i = 0; i < 100; i++)
                                iReceiptDate[i] = FromUnixTime(0);

                            //****

                            int[] iInEntryNo = new int[100];

                            //****
                            int ii = 0;

                            if (LotNo != null)
                            {
                                foreach (string st in LotNo)
                                {

                                    iLotNo[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (DescriptionLot != null)
                            {
                                ii = 0;
                                foreach (string st in DescriptionLot)
                                {

                                    iDescriptionLot[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (LotQty != null)
                            {
                                ii = 0;
                                foreach (int st in LotQty)
                                {

                                    iLotQty[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ExpirationDate != null)
                            {
                                ii = 0;
                                foreach (string st in ExpirationDate)
                                {

                                    iExpirationDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (ReceiptDate != null)
                            {
                                ii = 0;
                                foreach (string st in ReceiptDate)
                                {

                                    iReceiptDate[ii] = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }
                            }

                            if (InEntryNo != null)
                            {
                                ii = 0;
                                foreach (int st in InEntryNo)
                                {

                                    iInEntryNo[ii] = st;

                                    ii++;
                                    if (ii == 100)
                                        break;
                                }

                            }

                            bool result = false;

                            //iReadPost,iRoutingNo,istaplingNo,iWorkCenterNo,iTerminalName,iUserQuantity,iItemNo,iVariantCode,iBaseUnitOfMeasure
                            //iLotNo[string],iDescriptionLot[string]
                            //iLotQty[decimal]
                            //iExpirationDate[DateTime],iReceiptDate[DateTime]
                            //iInEntryNo[int]


                            result = myItem.SetComponentLotToHandle(ref ReadPost, RoutingNo, StaplingNo, WorkCenterNo, TerminalName, UserQuantity, ItemNo,
                                VariantCode, BaseUnitOfMeasure, ref iLotNo, ref iDescriptionLot, ref iLotQty, iExpirationDate, iReceiptDate, ref iInEntryNo);
                            


                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("ReadPost", ReadPost);

                            JArray oLotNo = new JArray();
                            foreach (string v in iLotNo)
                            {
                                oLotNo.Add(v);
                            }
                            oJsonObject.Add("oLotNo", oLotNo);

                            JArray oDescriptionLot = new JArray();
                            foreach (string v in iDescriptionLot)
                            {
                                oDescriptionLot.Add(v);
                            }
                            oJsonObject.Add("oDescriptionLot", oDescriptionLot);

                            JArray oLotQty = new JArray();
                            int indice = 0;
                            foreach (decimal v in iLotQty)
                            {
                                if (indice % 2 == 0)
                                    oLotQty.Add(v);
                                indice++;
                            }
                            oJsonObject.Add("oLotQty", oLotQty);
                            
                            JArray oExpirationDate = new JArray();
                            foreach (DateTime v in iExpirationDate)
                            {
                                oExpirationDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oExpirationDate", oExpirationDate);

                            JArray oReceiptDate = new JArray();
                            foreach (DateTime v in iReceiptDate)
                            {
                                oReceiptDate.Add(v.ToString("dd/MM/yyyy HH:mm:ss"));
                            }
                            oJsonObject.Add("oReceiptDate", oReceiptDate);

                            JArray oInEntryNo = new JArray();
                            foreach (int v in iInEntryNo)
                            {
                                oInEntryNo.Add(v);
                            }
                            oJsonObject.Add("oInEntryNo", oInEntryNo);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }

                /******************* WS_Manufacturing_Functions/PrepareLotComponentToRework  *******************/
                if (filename.CompareTo("/WS_Manufacturing_Functions/PrepareLotComponentToHandle") == 0)
                {

                    //WS_Manufacturing_Functions.ReqSessioneWork_Result a = new ReqSessioneWork_Result();

                    //WS_Manufacturing_Functions..WS_List_User_Work_Center[] newItemsInterface = new WS_List_User_Work_Center.WS_List_User_Work_Center[1];
                    WS_Manufacturing_Functions_PortClient myItem = new WS_Manufacturing_Functions_PortClient();

                    myItem.ClientCredentials.Windows.ClientCredential.Domain = "BCSPEAKERS";
                    myItem.ClientCredentials.Windows.ClientCredential.UserName = "appproduzione";
                    myItem.ClientCredentials.Windows.ClientCredential.Password = "Marzo2016";

                    myItem.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    int errore = 0;

                    try
                    {
                        Console.WriteLine(DateTime.Now + "|" + DateTime.Now.Ticks);

                        var results = JsonConvert.DeserializeObject<dynamic>(text);


                        string RoutingNo = results.RoutingNo;
                        string StaplingNo = results.StaplingNo;
                        string WorkCenterNo = results.WorkCenterNo;
                        string TerminalName = results.TerminalName;
                        string ItemNo = results.ItemNo;
                        string VariantCode = results.VariantCode;
                        string BaseUnitOfMeasure = results.BaseUnitOfMeasure;
                        string LocationCode = results.LocationCode;
                        string BinCode = results.BinCode;

                        decimal UserQuantity = results.UserQuantity;

                        if (errore == 0)
                        {

                            Console.WriteLine(RoutingNo + "|" + StaplingNo + "|" +
                                WorkCenterNo + "|" + TerminalName + "|" + UserQuantity + "|" +
                                ItemNo + "|" + VariantCode + "|" + BaseUnitOfMeasure);




                            bool result = false;

                            //iRoutingNo, iStaplingNo, iWorkCenterNo, iTerminalName, iItemNo , iVariantCode
                            //iBaseUnitOfMeasure, iLocationCode, iBinCode, iUserQuantity

                            result = myItem.PrepareComponentLotToHandle(RoutingNo, StaplingNo, WorkCenterNo, TerminalName, ItemNo,
                                VariantCode, BaseUnitOfMeasure, ref LocationCode, ref BinCode, UserQuantity);



                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("result", result);
                            oJsonObject.Add("LocationCode", LocationCode);
                            oJsonObject.Add("BinCode", BinCode);
                            oJsonObject.Add("UserQuantity", UserQuantity);


                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.OutputStream.Flush();



                        }
                        else
                        {
                            JObject oJsonObject = new JObject();

                            Stream input;
                            using (input = GenerateStreamFromString(oJsonObject.ToString()))
                            {

                                //Adding permanent http response headers
                                context.Response.ContentType = "application/json";
                                context.Response.ContentLength64 = input.Length;
                                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                                byte[] buffer = new byte[1024 * 16];
                                int nbytes;
                                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                                input.Close();
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.OutputStream.Flush();

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message + "|" + ex.Data.ToString() + "|" + ex.StackTrace + "");
                        ReturnException(context, ex);
                        context.Response.OutputStream.Flush();
                    }

                }
                else
                {
                    errPOST++;
                }


                if (errPOST == 22)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }
            }
            if (type == "OPTIONS")
            {
                
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.OutputStream.Flush();
                
            }
            if (type != "GET" && type != "POST" && type != "OPTIONS")
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }

            context.Response.OutputStream.Close();

        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private void Initialize(string path, int port)
        {
            this._rootDirectory = path;
            this._port = port;
            _serverThread = new Thread(this.Listen);
            _serverThread.Start();
        }

        public DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }
        
        public void ReturnException(HttpListenerContext context , Exception ex)
        {
            Stream input;
            using (input = GenerateStreamFromString(ex.Message))
            {

                //Adding permanent http response headers
                context.Response.ContentType = "application/json";
                context.Response.ContentLength64 = input.Length;
                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));

                byte[] buffer = new byte[1024 * 16];
                int nbytes;
                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                input.Close();
            }
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        }
    }
}
