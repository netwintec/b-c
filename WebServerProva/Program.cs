﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WebServerProva
{
    class Program
    {

        #region Nested classes to support running as service
        public const string ServiceName = "NavServer";

        public class Service : ServiceBase
        {
            public Service()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
                SimpleHTTPServer server = new SimpleHTTPServer("http://localhost", 8080);
            }

            protected override void OnStop()
            {
                //Program.Stop();
            }
        }
        #endregion
        static void Main(string[] args)
        {
            /*
            int number = 3;
            int row = 0;
            for (int i = 0; i < number*number; i++)
            {

                if(i>=(row*number) && i<((row * number)+row) && i!= 0)
                    Console.Write(" ");
                else
                    Console.Write("*");

                if ((i == (number * (row+1)) -1))
                {
                    Console.Write("\n");
                    row++;
                }
            }*/

            if (!Environment.UserInteractive)
                // running as service
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
            {
                SimpleHTTPServer server = new SimpleHTTPServer("http://localhost", 8080);
            }
            //SimpleHTTPServer server = new SimpleHTTPServer("http://localhost",8080);


        }
    }
}

