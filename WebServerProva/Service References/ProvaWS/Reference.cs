﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebServerProva.ProvaWS {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", ConfigurationName="ProvaWS.ProvaWs_Port")]
    public interface ProvaWs_Port {
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:microsoft-dynamics-schemas/codeunit/ProvaWs:TestWSARRAY", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        WebServerProva.ProvaWS.TestWSARRAY_Result TestWSARRAY(WebServerProva.ProvaWS.TestWSARRAY request);
        
        // CODEGEN: Verrà generato un contratto di messaggio perché l'operazione presenta più valori restituiti.
        [System.ServiceModel.OperationContractAttribute(Action="urn:microsoft-dynamics-schemas/codeunit/ProvaWs:TestWSARRAY", ReplyAction="*")]
        System.Threading.Tasks.Task<WebServerProva.ProvaWS.TestWSARRAY_Result> TestWSARRAYAsync(WebServerProva.ProvaWS.TestWSARRAY request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="TestWSARRAY", WrapperNamespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", IsWrapped=true)]
    public partial class TestWSARRAY {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayCodeuno")]
        public string[] wArrayCodeuno;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayText1")]
        public string[] wArrayText1;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=2)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayCode10")]
        public string[] wArrayCode10;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=3)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayText10")]
        public string[] wArrayText10;
        
        public TestWSARRAY() {
        }
        
        public TestWSARRAY(string[] wArrayCodeuno, string[] wArrayText1, string[] wArrayCode10, string[] wArrayText10) {
            this.wArrayCodeuno = wArrayCodeuno;
            this.wArrayText1 = wArrayText1;
            this.wArrayCode10 = wArrayCode10;
            this.wArrayText10 = wArrayText10;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="TestWSARRAY_Result", WrapperNamespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", IsWrapped=true)]
    public partial class TestWSARRAY_Result {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayCodeuno")]
        public string[] wArrayCodeuno;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayText1")]
        public string[] wArrayText1;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=2)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayCode10")]
        public string[] wArrayCode10;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/ProvaWs", Order=3)]
        [System.Xml.Serialization.XmlElementAttribute("wArrayText10")]
        public string[] wArrayText10;
        
        public TestWSARRAY_Result() {
        }
        
        public TestWSARRAY_Result(string[] wArrayCodeuno, string[] wArrayText1, string[] wArrayCode10, string[] wArrayText10) {
            this.wArrayCodeuno = wArrayCodeuno;
            this.wArrayText1 = wArrayText1;
            this.wArrayCode10 = wArrayCode10;
            this.wArrayText10 = wArrayText10;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ProvaWs_PortChannel : WebServerProva.ProvaWS.ProvaWs_Port, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ProvaWs_PortClient : System.ServiceModel.ClientBase<WebServerProva.ProvaWS.ProvaWs_Port>, WebServerProva.ProvaWS.ProvaWs_Port {
        
        public ProvaWs_PortClient() {
        }
        
        public ProvaWs_PortClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ProvaWs_PortClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ProvaWs_PortClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ProvaWs_PortClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WebServerProva.ProvaWS.TestWSARRAY_Result WebServerProva.ProvaWS.ProvaWs_Port.TestWSARRAY(WebServerProva.ProvaWS.TestWSARRAY request) {
            return base.Channel.TestWSARRAY(request);
        }
        
        public void TestWSARRAY(ref string[] wArrayCodeuno, ref string[] wArrayText1, ref string[] wArrayCode10, ref string[] wArrayText10) {
            WebServerProva.ProvaWS.TestWSARRAY inValue = new WebServerProva.ProvaWS.TestWSARRAY();
            inValue.wArrayCodeuno = wArrayCodeuno;
            inValue.wArrayText1 = wArrayText1;
            inValue.wArrayCode10 = wArrayCode10;
            inValue.wArrayText10 = wArrayText10;
            WebServerProva.ProvaWS.TestWSARRAY_Result retVal = ((WebServerProva.ProvaWS.ProvaWs_Port)(this)).TestWSARRAY(inValue);
            wArrayCodeuno = retVal.wArrayCodeuno;
            wArrayText1 = retVal.wArrayText1;
            wArrayCode10 = retVal.wArrayCode10;
            wArrayText10 = retVal.wArrayText10;
        }
        
        public System.Threading.Tasks.Task<WebServerProva.ProvaWS.TestWSARRAY_Result> TestWSARRAYAsync(WebServerProva.ProvaWS.TestWSARRAY request) {
            return base.Channel.TestWSARRAYAsync(request);
        }
    }
}
